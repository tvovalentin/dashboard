FROM python:3

WORKDIR /dashboard

# COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install dash

# COPY . .

CMD [ "python", "./dashboard.py" ]