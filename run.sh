#!/bin/bash

# Build Docker Image
docker build -t dashboard-img .

# Run Docker Container
sudo docker-compose up -d