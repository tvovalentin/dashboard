
from dash.dependencies import Input, Output
from dash import Dash, dcc, html, Input, Output
import os



external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.H1('Cool Dashboard')
])



if __name__ == '__main__':
    app.run_server(host='0.0.0.0', debug=True, port = 8051)